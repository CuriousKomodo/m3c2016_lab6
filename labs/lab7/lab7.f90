!lab7 code, parallel matrix multiplication
!To compile: gfortran -fopenmp -o test.exe lab7.f90
!To run: ./test.exe
!$ call omp_set_num_threads(2
!export OMP_NUM_THREADS=2

program lab7
    use omp_lib
    implicit none
    integer :: i1,j1,M,N,numThreads
    integer(kind=8) :: t1,t2,rate !timer variables
    real(kind=8) :: Csum
    real(kind=8), allocatable, dimension(:,:) :: A,B,C
real(kind=8), allocatable, dimension(:) :: temp


    !read in problem parameters
    open(unit=10,file='data.in')
        read(10,*) M
        read(10,*) N
!$      read(10,*) numThreads
    close(10)


    !initialize variables
    allocate(A(M,N),B(N,M),C(M,M))

!$ call omp_set_num_threads(numThreads)


!Task 2
!$OMP parallel
!$OMP sections
!$OMP section
call random_number(A)
!$OMP section
call random_number(B)
!$OMP end sections
!$OMP end parallel

call system_clock(t1)

Csum = 0.d0
!Task 3, 4
!$OMP parallel do reduction(+:Csum)
do i1=1,M
C(:,i1) = matmul(A,B(:,i1))
Csum = Csum + sum(C(:,i1))
end do
!$OMP end parallel do

call system_clock(t2,rate)

print *, Csum,sum(C)
print *, 'test C:',maxval(abs(C-matmul(A,B)))
print *, 'test Csum:',(Csum-sum(C))/Csum
print *, 'wall time:',float(t2-t1)/float(rate)





end program lab7




end program lab7
