

program test_network
use network
use netstats
use omp_lib

implicit none
integer, parameter :: n0=5,l=2,nt=400,m=200,numThreads=1
integer, dimension(n0+nt,m) :: qnetm
integer :: qmaxm
real(kind=8) :: qvarm, ave_walltime
integer, dimension(m) :: qmaxvec


!subroutine calls below should be modified as required and variables should be declared above as needed.
!call stats(n0,l,nt,m,qnetm,qmaxm,qvarm)
!print *, 'qmaxm=', qmaxm
!print *, 'qvarm=', qvarm
!print *, 'qmaxvec=', qmaxvec
!print *, 'qvarm =', qvarm

call stats_omp(n0,l,nt,m,numThreads,qnetm,qmaxm,qvarm)

call test_stats_omp(n0,l,nt,m,numThreads,ave_walltime)
print *, ave_walltime
end program test_network

