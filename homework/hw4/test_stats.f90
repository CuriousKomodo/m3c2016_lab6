module teststats
use network
use omp_lib

contains


subroutine test_stats_omp(n0,l,nt,m,numThreads,ave_walltime)
!Input: same as stats_omp
!Output: walltime: time for 100 cals to stats_par
    implicit none
    integer, intent(in) :: n0,l,nt,m,numThreads
    real(kind=8), intent(out) :: ave_walltime
    integer :: i, ta, tb, clock_rate
    real(kind=8), dimension(100) :: walltime
    integer, dimension(n0+nt,m) :: qnetm
    integer :: qmaxm
    real(kind=8) :: qvarm

    if (numThreads==1) then
        call system_clock(ta,clock_rate)
        do i=1,100
        call stats(n0,l,nt,m,qnetm,qmaxm,qvarm)
    end do
    call system_clock(tb, clock_rate)
    ave_walltime = dble(tb-ta)/(dble(clock_rate)*100)

    elseif (numThreads>1) then
        call system_clock(ta,clock_rate)
        do i=1,100
            call stats_omp(n0,l,nt,m,numThreads,qnetm,qmaxm,qvarm)
        end do
        call system_clock(tb, clock_rate)
        ave_walltime = dble(tb-ta)/(dble(clock_rate)*100)
    end if

    print *, 'ave_walltime=', ave_walltime
end subroutine test_stats_omp

end module teststats
