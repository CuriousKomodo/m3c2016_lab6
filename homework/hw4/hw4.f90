!Template code for hw4
!Should be compiled with network.f90
module netstats
	use network
	use omp_lib

	contains

subroutine stats(n0,l,nt,m,qnetm,qmaxm,qvarm)
	!Input: 
	!n0,nl,nt: recursive network model parameters
	!m: number of network realizations to compute
	!Output:
	!qnetm: node lists for all m networks
	!qvarm: ensemble averages of var(q)
	!qmaxm: maximum qmax from m network realizations
    implicit none
    integer, intent(in) :: n0,l,nt,m
    integer, dimension(n0+nt,m), intent(out) :: qnetm
    integer, intent(out) :: qmaxm
    real(kind=8), intent(out) :: qvarm
    integer :: i1, j1, qmax
    integer, dimension(n0+nt) :: qnet
    integer, dimension(n0+l*nt,2) :: enet
    integer, dimension(m) :: qmaxvec
    real(kind=8), dimension(m) :: qvarvec, qmeanvec
    real(kind=8), dimension(n0+nt) :: square_diff

    !initialize qmaxm
    !Loop from 1 to m, using generate function m times.
    !In each itereation,mean(qnet_i1),var(qnet_il) are calculated, and qmaxm is compared step by step.
    do i1 = 1,m
        call generate(n0,l,nt,qmax,qnet,enet)
        qnetm(:,i1) = qnet
        !find average in each column, hence:
        qmeanvec(i1) = (sum(qnet))/(n0+nt)
        qmaxvec(i1) = qmax!This step finds the maximum of qmax
        ! loop through each term in this i1 th qnet, to calculate variance
        !
        do j1 = 1,n0+nt
            square_diff(j1) = (qnet(j1)-qmeanvec(i1))**2
        end do
        qvarvec(i1) = sum(square_diff)/(n0+nt)
    end do
    !calculate mean(var(qnet_i1))
    !61, 20.777
    !Calculate qvarm, which is the average of variance
    qvarm = sum(qvarvec)/m
    qmaxm = maxval(qmaxvec)

end subroutine stats


subroutine stats_omp(n0,l,nt,m,numThreads,qnetm,qmaxm,qvarm)
!Input:
!n0,nl,nt: recursive network model parameters
!m: number of network realizations to compute
!numThreads: number of threads for parallel computation
!Output:
!qnetm: node lists for all m networks
!qvarm: ensemble averages of var(q)
!qmaxm: maximum qmax from m network realizations
    implicit none
    integer, intent(in) :: n0,l,nt,m,numThreads
    real(kind=8), intent(out) :: qvarm
    integer, dimension(n0+nt,m), intent(out) :: qnetm
    integer, intent(out) :: qmaxm
    integer :: i1, j1, qmax
    integer, dimension(n0+nt) :: qnet
    integer, dimension(n0+l*nt,2) :: enet
    integer, dimension(m) :: qmaxvec
    real(kind=8), dimension(m) :: qvarvec, qmeanvec
    real(kind=8) :: qvar,partial_qvar

!$call omp_set_num_threads(numThreads)

    qvar = 0.0 !initialize?
!$OMP parallel do private(j1,qnet,qmax,enet,qvar)
    do i1 = 1,m
        call generate(n0,l,nt,qmax,qnet,enet)
        qnetm(:,i1) = qnet
!find average in each column, hence:
        qmeanvec(i1) = (sum(qnet))/(n0+nt)
        qmaxvec(i1) = qmax!This step finds the maximum of qmax
        !$OMP parallel do private(partial_qvar), reduction(+:qvar)
        do j1 = 1,n0+nt
            partial_qvar=(qnet(j1) - qmeanvec(i1))**2
            qvar=partial_qvar+qvar
        end do
        !OMP end parallel do
        qvarvec(i1) = qvar/(n0+nt)
    end do
    !$OMP end parallel do
    qvarm = sum(qvarvec)/m
    qmaxm = maxval(qmaxvec)
    print *, 'qvarm_parallel=', qvarm
    print *, 'qmaxm_parallel=', qmaxm
end subroutine stats_omp

end module netstats
