"M3C Homework 4" 
"Kefei Hu"
"00931440" 
import numpy as np
import matplotlib.pyplot as plt
from ns3 import netstats as ns #assumes netstats module has been compiled as ns.so with f2py


def convergence(n0,l,nt,m,numthreads,display=False): 
    #the array, mvec defines the range of m values for iteration
    #assuming m is multiple of 10
    mvec = np.linspace(10,m,m/10)
    size = np.size(mvec)
    qmax_ave = np.zeros(size)
    #stores mean maximum degree number for different values of m
    #loop through values in the m-array
    for k in range(size):
        [qnetm, qmaxm, qvarm] = ns.stats(n0,l,nt,mvec[k])
        #finding maximum of each column in qnetm, returning these qmaxes as a k-sized array called localmax
        localmax = np.amax(qnetm,axis=0) 
        #calculate the mean of the k-group of qmaxes, store in array qmax_ave.  
        qmax_ave[k] = np.mean(localmax)
    #Q = Am^(-k) => log(Q) = log(A) - k*log(m)
    #Hence apply polyfit to log(qmax_ave) and log(m), to an order of 1.
    polynomial = np.polyfit(np.log(mvec),np.log(qmax_ave),1)
    A = np.exp(polynomial[1])
    k = polynomial[0]
        
    if display==True: 
        plt.clf()
        plt.figure()
        plt.loglog(mvec,qmax_ave, 'x')
        plt.loglog(mvec,A*mvec**(k), 'k--')
        plt.xlabel('m')
        plt.ylabel('qmax_ave')
        plt.title('qmax_ave versus m=1:%d, with n0=%d, l=%d and nt=%d \n (Kefei Hu, convergence)' %(m,n0,l,nt))
        plt.legend('actual','estimate with k=' %-k)
        
        plt.savefig('hw41') 
    return k   

		
def speedup(n0,l,ntarray,marray):
	"""test speedup of stats_par"""
	n = int(np.size(ntarray)) 
	m = int(np.size(marray) )
	speedup_n = np.zeros(n)
	speedup_m = np.zeros(m)
	print n
	print m
	mfix = 100 #fixing value of m for figure 1
	nfix = 100 #fixing value of n for figure 2
	
	#loop through values in ntarray from 10 to 400, while keeping the value of m at 100. 
	for i in range(n): 
	   walltime = ns.test_stats_omp(n0,l,ntarray[i],mfix,1)
	   walltime_omp = ns.test_stats_omp(n0,l,ntarray[i],mfix,2)
	   #Express the speedup in terms of the ratio walltime:walltime_omp. 
	   #Greater the value of the ratio, more efficient the parallelisation. 
	   speedup_n[i] = walltime/walltime_omp
	   print speedup_n[i]
	print speedup_n
	
	
	#Similarly, loop through values in marray from 10 to 400, while keeping the value of nt at 100
	for j in range(m):
	   walltime = ns.test_stats_omp(n0,l,nfix,marray[j],1)
	   walltime_omp = ns.test_stats_omp(n0,l,nfix,marray[j],2)
	   speedup_m[j] = walltime/walltime_omp
	   print speedup_m[j]   
	
	plt.clf()
	plt.figure(1)
	plt.plot(ntarray,speedup_n)
	plt.title('Ratio of speedup on walltime against nt at m=%d \n' %mfix )
	plt.xlabel('nt')
	plt.ylabel('ratio')
	plt.savefig('hw421')
	plt.show()
	
	
	plt.clf()
	plt.figure(2)
	plt.plot(ntarray,speedup_m)
	plt.title('Ratio of speed-up on walltime against m at n=%d \n' %nfix)
	plt.xlabel('m')
	plt.ylabel('%speed-up')
	plt.savefig('hw422')
	plt.show
	
	#Comment: 
	#In general, parallelization gives significantly shorter walltime for nt and mt > 10.
	#For small values of nt and mt, the speed-up ratio is unstable, hence this is not included in the plots. 
	#As the value of nt increases, the speed-up ratio by parallelization increases. But as nt gets larger, the speed-up ratio tends to around 2.8.
        #As the value of m incrases, the speed-up ratio by parallelization increases. But as m gets larger, the speed-up ratio tends to around 2.5.   
#Set n0=5 and l=2, and explore how nt and m influence the speedup. 

def degree_average(n0,l,nt,m,display=False):
    #compute ensemble-averaged degree distribution	
    
    [qnetm, qmaxm, qvarm]=ns.stats_omp(n0,l,nt,m,2)
    #qnetm stores the degree-list for all m networks. 
    #I could iterate through each column of qnetm, to find average counts per degree number for each network, and then sum them up
    #But then that is equivalent to finding counts per degree number in the entire matrix
    #Create a vector, d = (1,2,..., qmaxm), defining the range of possible degree numbers to look for in qnetm
     
    d = np.arange(0,qmaxm+1);
    degree_counts = np.zeros(qmaxm+1)
    #loop through each term in d,using bincount to find counts for each degree number in qnetm 
    #The counts per degree number is stored in degree_counts
    for i in d:
        degree_counts[i-1] = np.bincount(qnetm.ravel())[i]
    #Remove zero-elements in degree_counts and normalize, to obtain degree distribution P(d)    
    #Remove the degree numbers from d-array, where P(d) = 0
    P = degree_counts[degree_counts!=0]/(1.0*m*(n0+nt))
    d = d[degree_counts!=0]
    
    #Since P(d) follows a power-law, then assume P(d) = C*d^q, where q<0
    #Take log on both sides, log(P) = log(C)+q*log(d) = c+q*log(d), where c=log(C)
    q,c = np.polyfit(np.log(d),np.log(P),1)
    
    #Make a log-log plot of this distribution if display is True. 
    
    if display:
        plt.figure()
        plt.loglog(d,P,'x')
        plt.loglog(d,np.exp(c)*d**q,'k--')
        plt.xlabel('degree,d')
        plt.ylabel('P(d)')
        plt.title('Degree distribution, over m networks with N0,L,Nt=%d,%d,%d \n (degree_average, Kefei Hu)' %(n0,l,nt))
        plt.legend(('Computed','Fit, P ~ d^-k with k=%f' %(-q)))
        plt.show()
    
    return q
    

if __name__ == '__main__':
#Add code to here to call functions above
    n0,l,nt,1000 = 5,2,400,1000
    k = convergence(n0,l,nt,m,numthreads,display=False)
    q = degree_average(n0,l,nt,m,display=True)