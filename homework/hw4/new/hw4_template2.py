"""template code for hw4
"""
import numpy as np
import matplotlib.pyplot as plt
from ns import netstats as ns #assumes netstats module has been compiled as ns.so with f2py


def convergence(n0,l,nt,m,numthreads,display=False):
    """test convergence of qmax_ave"""
    """loops stats for different values of m"""
    qmax_ave = zeros(m) #stores mean maximum degree number for different values of m
    mvec = np.arrnge(1,m+1)
    for num_net in mvec:
        ns.stats(n0,l,nt,num_net)
        localmax = np.amax(ns.qnetm,axis=1) #num_net size row vector
        qmax_ave(num_net-1) = np.mean(localmax)
        
    if display:
        plt.figure()
        np.loglog(mvec,qmax_ave)
        plt.savefig('hw41')
        
	polynomial = np.polyfit(log(mvec),log(qmax_ave),1)
	k=-1*polynomial[0]
    return k
	
def speedup(n0,l,ntarray,marray):
	"""test speedup of stats_par"""
	


def degree_average(n0,l,nt,m,display=False):
	"""compute ensemble-averaged degree distribution"""	




#if __name__ == '__main__'
#Add code to here to call functions above
(n0,l,nt) = 5,2,400
m=1000
convergence(n0,l,nt,m,numthreads,display=False)