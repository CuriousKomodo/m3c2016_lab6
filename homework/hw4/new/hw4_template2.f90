!Template code for hw4
!Should be compiled with network.f90
module netstats
	use network
	use omp_lib

	contains

subroutine stats(n0,l,nt,m,qnetm,qmaxm,qvarm)
	!Input: 
	!n0,nl,nt: recursive network model parameters
	!m: number of network realizations to compute
	!Output:
	!qnetm: node lists for all m networks
	!qvarm: ensemble averages of var(q)
	!qmaxm: maximum qmax from m network realizations
	implicit none
	integer, intent(in) :: n0,l,nt,m
	integer, dimension(n0+nt,m), intent(out) :: qnetm
    integer, dimension(n0+nt) :: qnet, part_var
     integer, dimension(m) :: mean_qnet, var_qnet
	integer, intent(out) :: qmaxm
	real(kind=8), intent(out) :: qvarm

    !initialize qmaxm
    qmaxm = 0
    !Loop from 1 to m, using generate function m times. 
    !In each itereation,mean(qnet_i1),var(qnet_il) are calculated, and qmaxm is compared step by step.
    do i1 = 1,m
        call generate(n0,l,nt)
        qnetm(:,i1) = qnet
        !find average in each column, hence: 
        mean_qnet(i1) = sum(qnet)/(n0+nt)

        ! loop through each term in this i1 th qnet, to calculate variance
        do j1 = 1,n0+nt
            part_var(j1) = (qnet(j1)-mean_qnet)^2
        end do
        var_qnet(i1) = sum(part_var)
        qmaxm = maxval(qmax,qmaxm)!This step finds the maximum of qmax
    end do
    !calculate mean(var(qnet_i1))

!Calculate qvarm, which is the average of variance
qvarm = sum(var_qnet)/m

print *, 'mean variance across m networks = ', qvarm
print *, 'maximum degree number across m networks = ', qmaxm

end subroutine stats

end module netstats
