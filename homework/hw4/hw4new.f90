!M3C Homework 4
!Kefei Hu
!00931440


module netstats
	use network
	use omp_lib

	contains

subroutine stats(n0,l,nt,m,qnetm,qmaxm,qvarm)
	!Input: 
	!n0,nl,nt: recursive network model parameters
	!m: number of network realizations to compute
	!Output:
	!qnetm: node lists for all m networks
	!qvarm: ensemble averages of var(q)
	!qmaxm: maximum qmax from m network realizations
    implicit none
    integer, intent(in) :: n0,l,nt,m
    integer, dimension(n0+nt,m), intent(out) :: qnetm
    integer, intent(out) :: qmaxm
    real(kind=8), intent(out) :: qvarm
    integer :: i1, j1, qmax
    integer, dimension(n0+nt) :: qnet
    integer, dimension(n0+l*nt,2) :: enet
    integer, dimension(m) :: qmaxvec
    real(kind=8), dimension(m) :: qvarvec, qmeanvec

    !initialize qmaxm
    !Loop from 1 to m, using generate function m times.
    !In each itereation,mean(qnet_i1),var(qnet_il) are calculated, and qmaxm is compared step by step.
    do i1 = 1,m
        call generate(n0,l,nt,qmax,qnet,enet)
        qnetm(:,i1) = qnet
        !find average in each column, hence:
        qmeanvec(i1) = dble((sum(qnet))/(n0+nt))
        qmaxvec(i1) = qmax!This step finds the maximum of qmax
        qvarvec(i1) = dble(dot_product(qnet,qnet))/(n0+nt)-dble(qmeanvec(i1)**2)
    end do
    !calculate mean(var(qnet_i1))
    !61, 21.777
    !Calculate qvarm, which is the average of variance
    qvarm = dble(sum(qvarvec))/m
    qmaxm = maxval(qmaxvec)

end subroutine stats


subroutine stats_omp(n0,l,nt,m,numThreads,qnetm,qmaxm,qvarm)
!Input:
!n0,nl,nt: recursive network model parameters
!m: number of network realizations to compute
!numThreads: number of threads for parallel computation
!Output:
!qnetm: node lists for all m networks
!qvarm: ensemble averages of var(q)
!qmaxm: maximum qmax from m network realizations
    implicit none
    integer, intent(in) :: n0,l,nt,m,numThreads
    real(kind=8), intent(out) :: qvarm
    integer, dimension(n0+nt,m), intent(out) :: qnetm
    integer, intent(out) :: qmaxm
    integer :: i1, j1, qmax
    integer, dimension(n0+nt) :: qnet
    integer, dimension(n0+l*nt,2) :: enet
    integer, dimension(m) :: qmaxvec
    real(kind=8), dimension(m) :: qvarvec, qmeanvec
    real(kind=8) :: varsum

!$call omp_set_num_threads(numThreads)

    qvarm = 0.0 !initialize?
!$OMP parallel do private(qnet,qmax,enet), reduction(+:varsum)
    do i1 = 1,m
        call generate(n0,l,nt,qmax,qnet,enet)
        qnetm(:,i1) = qnet
        qmeanvec(i1) = dble((sum(qnet))/(n0+nt))
        qmaxvec(i1) = qmax!This step finds the maximum of qmax
        qvarvec(i1) = dble(dot_product(qnet,qnet))/(n0+nt)-dble(qmeanvec(i1)**2)
        varsum =varsum+ qvarvec(i1)
    end do
    !$OMP end parallel do
    qvarm = varsum/m
    qmaxm = maxval(qmaxvec)
end subroutine stats_omp


subroutine test_stats_omp(n0,l,nt,m,numThreads,ave_walltime)
!Input: same as stats_omp
!Output: walltime: time for 100 cals to stats_par
    implicit none
    integer, intent(in) :: n0,l,nt,m,numThreads
    real(kind=8), intent(out) :: ave_walltime
    integer :: i, ta, tb, clock_rate
    real(kind=8), dimension(100) :: walltime
    integer, dimension(n0+nt,m) :: qnetm
    integer :: qmaxm
    real(kind=8) :: qvarm

    if (numThreads==1) then
        call system_clock(ta,clock_rate)
        do i=1,100
            call stats(n0,l,nt,m,qnetm,qmaxm,qvarm)
        end do
        call system_clock(tb, clock_rate)
    ave_walltime = dble(tb-ta)/(dble(clock_rate)*100)

    elseif (numThreads>1) then
        call system_clock(ta,clock_rate)
        do i=1,100
            call stats_omp(n0,l,nt,m,numThreads,qnetm,qmaxm,qvarm)
        end do
        call system_clock(tb, clock_rate)
    ave_walltime = dble(tb-ta)/(dble(clock_rate)*100)
    end if
end subroutine test_stats_omp



end module netstats
