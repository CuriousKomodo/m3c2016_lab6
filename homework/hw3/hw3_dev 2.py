print 'M3C - Homework 3'
print 'Kefei Hu'
print '00931440'

import numpy as np
#print numpy.__path__
import matplotlib.pyplot as plt
#import networkx as nx
from n1 import network as net

#cannt use networkx
    
def visualise(e):
    maxnode = np.amax(e) +1 
    randx = np.random.rand(maxnode)
    randy = np.random.rand(maxnode)
    numEdge = e.shape[0]
    for i in range (numEdge): 
        node1 = e[i,0]
        node2 = e[i,1]
        plt.setp(plt.plot([randx[node1], randx[node2]], [randy[node1], randy[node2]]),linewidth=0.2,marker='o')
    plt.show()
    
    
    
def degree(N0,L,Nt,display=False):
    """Compute and analyze degree distribution based on degree list, q,
    corresponding to a recursive network (with model parameters specified
    as input."""
    '''use this to generate enet, gnet from net.generates(N0,L,Nt)'''
    #Obtain enet and gnet from Fortran
    max_degree = net.generate(N0, L, Nt) 
    edge_list = net.enet
    degree_list = np.array(net.qnet)
    #find total number of nodes
    N_nodes = np.size(degree_list)
    #intialize arrays to store number of degrees and P(d)
    degree_count = np.zeros(max_degree) 
    P = np.zeros([max_degree,])
    #d=array containing all degree numbers
    d = np.arange(1, max_degree+1)
    #loop through degrees from 0 to the maximum degree
    for i in d:
        a=degree_list[degree_list==i]
        degree_count[i-1] = (np.sum(a)+0.0)/i
        P[i-1]=degree_count[i-1]/N_nodes
    #print P
    d = d[P!=0]; P = P[P!=0]
    if display:
        #plot from k=1 to k=qmax
        plt.figure()
        plt.plot(d, P,'.', label = 'a')
        plt.title('Kefei Hu, degree distribution P(d)')
        plt.xlabel('d')
        plt.ylabel('P(d)')
        # Discard the second half of P  
	#discard the upper half part of P and degree
   	P_half = P[P<int(max_degree/2)]
   	d_half = d[d<int(max_degree/2)]
	P_array = P[P_half!=0]
	d_array = d[P_half!=0]
   	logd = np.log(d_array)
	logP = np.log(P_array)
    #Discard the upper half of the degree distribution
    #Also discard the entries on P where P=0
    #Select values of P where P(d) is not zero, then select the corresponding degree, d
    #Then take log on both array!
   	halflength = logP.shape[0]
 	#Assume P is a scale-free network and follows the power law, P(k) = A*k^(-c)
    	#Use polyfit on log(P) with respect to log(d) to obtain array (a_1, a_0), up to an order = 1
    	#a_1 = -c, a_0 = log(A)
	print logd.shape
	print logP.shape
    	polynomial = np.polyfit(logd,logP,1)
    	constant = np.exp(polynomial[1])
    	fit = constant*pow(k,polynomial[0])#
    	plt.plot(d,fit,label='fit')
    	plt.show()
    	power = np.ones(number_degrees) * polynomial[0]
	print 'polynomial=', polynomial
 	plt.figure()
        plt.plot(d, P,'.', label = 'a')
 	plt.plot(d, fit, label='b')
        plt.title('Kefei Hu, degree distribution P(d) and its estimation')
        plt.xlabel('degree')
        plt.ylabel('P(d)')
        plt.savefig(hw3)
    return P,d

