program test_network
	use network
	implicit none


!subroutine calls below should be modified as required and variables should be declared above as needed.
    integer :: qmax
    real(kind=8) :: c
    real(kind=8), allocatable, dimension(:) :: carray
    call generate(5,3,2,qmax)
    call adjacency_matrix()
    !call adjacency_matrix()
    !call output_anet()
    !call output_enet()
    !call output_q()
    call connectivity(c)
    !call vary_connectivity(5,3,2,carray)


end program test_network



