"""Template code for M3C 2016 Homework 3"""
import numpy as np
print numpy.__path__
import matplotlib.pyplot as plt
#import networkx as nx
from n3 import network as net

#cannt use networkx
def visualize(e):   
    nodes = np.unique(e)
    '''loop through matrix to obtain nodes at random coordinates, then edges
    '''
    !make a matrix (n,2) where n=number of nodes
    !make each entry completely random
    ! coordinate of node_ i = matrix(i,2)
    G=nx.Graph()
    for i in nodes:
        x= np.random.uniform(0, 1)
        y= np.random.uniform(0, 1)
        G.add_node(i,pos=(x,y))
    for i in range (e.shape[0]-1):
        G.add_edge(e[i,0],e[i,1], width = 0.2)
    nx.draw(G)


"""Visualize a network represented by the edge list
contained in e
"""

def degree(N0,L,Nt,display1=False, display2=False):
    """Compute and analyze degree distribution based on degree list, q,
    corresponding to a recursive network (with model parameters specified
    as input."""
    '''use this to generate enet, gnet from net.generates(N0,L,Nt)'''
    #Obtain enet and gnet from Fortran
    max_degree = net.generate(N0, L, Nt) 
    edge_list = net.enet
    degree_list = np.array(net.qnet)
    #find total number of nodes
    N_nodes = np.size(degree_list)
    #intialize arrays to store number of degrees and P(d)
    degree_count = np.zeros(max_degree) 
    P = np.zeros(max_degree)
    d = np.unique(net.qnet) 
    #loop through degrees from 0 to the maximum degree
    for i in np.arange(1,max_degree+1):
        a=degree_list[degree_list==i]
        degree_count[i-1] = np.sum(a)/i
        P[i-1]=degree_count[i-1]/N_nodes
    return d, P
    print d,P
    if display1 == True:
        #plot from k=1 to k=qmax
        plt.figure()
        plt.plot(d, P)
        plt.title('Kefei Hu, degree distribution P(d)')
        plt.xlabel('d')
        plt.ylabel('P(d)')
        plt.show()
    # Discard the second half of P  

    number_degrees = np.shape(P)
    P = degree(N0,L,Nt)
    logd = np.log(np.arange(1,number_degrees+1))
    max_degree = net.generate(N0,L,Nt)
    d = np.arange(1,max_degree+1)
    #Discard the upper half of the degree distribution
    #Also discard the entries on P where P=0
    P_half = P[P<np.floor(max_degree/2)]
    d_half = d[d<np.floor(max_degree/2)]
    #Select values of P where P(d) is not zero, then select the corresponding degree, d
    #Then take log on both arrays
    P_array = P_half[P_half !=0]
    d_array = d_half[P_half !=0]
    logP_array = log(P_array)
    logd_array = log(d_array)
    halflength = size(logd_half)
    
    #Assume P is a scale-free network and follows the power law, P(k) = A*k^(-c)
    #Use polyfit on log(P) with respect to log(d) to obtain array (a_1, a_0), up to an order = 1
    #a_1 = -c, a_0 = log(A)
    polynomial = np.polyfit(logd_half,logP_half,1)
    constant = np.exp(polynomial[1])
    power = np.ones(number_degrees) * polynomial[0]
    Pestimate = constant * np.power(logd, power)
    
    #Pestimate = np.zeros(number_degrees,1)
    #for i in np.arange(1,number_degrees+1):
        #Pestimate(i) = constant * np.power(i,3)
    if display2 == True:
        plt.figure()
        plt.plot(np.arange(1,number_degrees), P,label='a')
        plt.plot(np.arange(1,number_degrees), Pestimate, label='b')
        plt.title('Comparing P(d) with estimation')
        plt.xlabel('d')
        plt.ylabel('P(d)')
        plt.savefig(hw3)
    return(P,np.arange(1,max_degree+1))
   
   