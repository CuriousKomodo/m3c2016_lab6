module network
    implicit none
    integer, allocatable, dimension(:) :: qnet
    integer, allocatable, dimension(:,:) :: anet
    integer, allocatable, dimension(:,:) :: enet
    save
contains


!--------------------------------------------------------
subroutine generate2(N0,L,Nt,qmax)
	!Generate recursive matrix corresponding
	!to model parameters provided as input
    implicit none
    integer, intent(in) :: N0,L,Nt
    integer, intent(out) :: qmax
    integer :: i1,i2,j,sum_weight
    real(kind=8) :: x
    real(kind=8), allocatable, dimension(:) :: weight


    if (.not. allocated(qnet)) allocate(qnet(N0+Nt))
    if (.not. allocated(enet)) allocate(enet(N0+L*Nt,2))
    qnet = 0
    enet = 0
    do i1=1,N0-1
        enet(i1,1)=i1
        enet(i1,2)=i1+1
        qnet(i1)=2
    end do
    enet(N0,1)=N0
    enet(N0,2)=1
    qnet(N0)=2

    allocate(weight(N0+Nt))
    do i1=1,Nt
        sum_weight=sum(qnet(1:N0+Nt))
        qnet(i1+N0)=L
        do i2=1,N0+Nt
            weight(i2)=sum(qnet(1:i2))/dble(sum_weight)
        end do
        do j=1,L
            x = rand()
            do i2=1,N0+Nt
                if (x<weight(i2)) then
                     enet(N0+(i1-1)*L+j,1) = N0+i1
                     enet(N0+(i1-1)*L+j,2) = i2
                     qnet(i2) = qnet(i2)+1
                     exit
                end if
            end do
       end do
    end do
    deallocate(weight)

    qmax=maxval(qnet)

end subroutine generate2

!--------------------------------------------------------
subroutine adjacency_matrix()
	!Create adjacency matrix corresponding
	!to pre-existing edge list
    implicit none
    integer :: i1,x,y,N

    N=maxval(enet)
    if (.not. allocated(anet)) allocate(anet(N,N))
    anet = 0
    do i1=1,N
        x=enet(i1,1)
        y=enet(i1,2)
        anet(x,y)=anet(x,y)+1
        anet(y,x)=anet(y,x)+1
    end do

end subroutine adjacency_matrix


!------------------------------------------------------
subroutine connectivity(c)
	!Compute connectivity of pre-computed network
	implicit none
	real(kind=8), intent(out) :: c
    integer :: s,i1,INFO,LWORK
    real(kind=8), allocatable, dimension(:,:) :: M
    real(kind=8), allocatable, dimension(:) :: W,WORK

    s=int(size(qnet))
    allocate(M(s,s))
    M=0

    do i1=1,s
        M(i1,i1)= qnet(i1)
    end do
    M = M - anet
    M = M * 1.d0


    !finding optimal size of LWORK
    LWORK=-1
    allocate(WORK(1),W(s))
    call dsyev('N','U',s, M, s, W, WORK,LWORK,INFO)
    LWORK=int(WORK(1))
    deallocate(WORK,W)

    !finding eigenvalues
    allocate(WORK(LWORK),W(s))
    call dsyev('N','U',s, M, s, W, WORK,LWORK,INFO)
    c=W(2)
    deallocate(M,W,WORK)

end subroutine connectivity


!-------------------------------------------------------
subroutine vary_connectivity(N0,Lmax,Nt,carray)
	! Compute connectivity for networks with L varying
	! between 1 and Lmax

    ! Output: carray = 6.7304688034108113E-004  0.86493412589676177        1.9642647109493849 3.4269712498421172        4.6975077468879531        5.8827139530876433
    ! Observation: eigenvalue increases as number of connection (L) increases. Moreover, these values are always non-negative, because connectivity cannot be negative.

	implicit none
	integer, intent(in) :: N0,Lmax,Nt
    integer :: i1,qmax
    real(kind=8) :: c
	real(kind=8), dimension(Lmax),intent(out) :: carray

    do i1=1,Lmax
        call generate(N0,i1,Nt,qmax)
        call adjacency_matrix()
        call connectivity(c)
        carray(i1)=c
        deallocate(qnet,anet,enet)
    end do

end subroutine vary_connectivity


end module network
