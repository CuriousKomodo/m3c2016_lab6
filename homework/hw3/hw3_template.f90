!Template code for Homework 3
! Kefei Hu
module network

    implicit none
    integer, allocatable, dimension(:) :: anet, qnet, enet, nodecopy
    integer, allocatable, dimension(:,:) :: enet
    save
contains
!nodecopy stores copies of the node according their relative probability at any given time, updated in the loop
!need to call programs by, real(unit=8), external :: functionname
!P_i at a chosen time= q_i/sum(q_i's)

subroutine generate(N0,L,Nt,qmax)
	!Generate recursive matrix corresponding
	!to model parameters provided as input
	implicit none
	integer, intent(in) :: N0,L,Nt
	integer, intent(out) :: qmax
    integer :: i1, t_i, link_i
    integer, allocatable, dimension(:) :: node_array
    real(kind=8), dimension(L) :: randarray

    do i1 = 1,N0
        qnet(i1) = 2
    end do
!initialize enet
    do i1 =1,N0-1
        enet(i1,:) = (/i1,i1+1/)
    end do
    enet(N0,:) = (/N0,1/)
print *, 'enet=', enet
print *, 'qnet=', qnet
!----------------------------finish initialization, onto loops
!Now form the replication vector, form nodecopy

    do t_i = 1,Nt
        !add one more node, but wait till edge gets updated
        node_array = ((t_i, j=1:qnet(i2)), t_i =1,size(qnet))
        !random vector L, with numbers from 0 to 1
        allocate(randarray(L))
        call random_seed
        call random_number(randarray)

        do link_i = 1,L
            node_index = floor(randarray(link_i)*size(node_array)) + 1
            qnet(node_index) = qnet(node_index) +1
            enet(N0+(t_i-1)*L+link_i,:) = (/node_index(link_i), N0+t_i/)
        end do
    end do

    qmax = maxval(qnet)

end subroutine generate


subroutine adjacency_matrix()
	!Create adjacency matrix corresponding 
	!to pre-existing edge list 
    !Initialize the anet matrix
implicit none
integer, allocatable, dimension(:,:), intent(in) :: enet
integer, allocatable, dimension(:,:), intent(out) :: anet
integer, allocatable, dimension(:,:) :: size_enet
integer :: i, node1, node2

    size_enet=size(enet)
    anet = 0.0d
!loop through the edge list
    do i  = 1,size_enet(1)
        node1 = enet(i,1)
        node2 = enet(i,2)
        anet(node1,node2) = 1
        anet(node2,node1) = 1
    end do
print *, 'anet=', anet
end subroutine adjacency_matrix







subroutine connectivity(c)
	!Compute connectivity of pre-computed network
	implicit none
	real(kind=8), intent(out) :: c

end subroutine connectivity


subroutine vary_connectivity(N0,Lmax,Nt,carray)
	!Compute connectivity for networks with L varying
	!between 1 and Lmax
	implicit none
	integer, intent(in) :: N0,Lmax,Nt
	real(kind=8), dimension(Lmax),intent(out) :: carray


end subroutine vary_connectivity()


end module network
