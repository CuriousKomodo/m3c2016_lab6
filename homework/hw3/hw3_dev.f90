!M3C - Homework 3
! Kefei Hu
! 00931440
module network

implicit none
integer, allocatable, dimension(:) :: qnet, nodeArray
integer, allocatable, dimension(:,:) :: enet, anet
save
contains
!nodecopy stores copies of the node according their relative probability at any given time, updated in the loop
!need to call programs by, real(unit=8), external :: functionname
!P_i at a chosen time= q_i/sum(q_i's)

subroutine generate(N0,L,Nt,qmax)
!Generate recursive matrix corresponding
!to model parameters provided as input
implicit none
integer, intent(in) :: N0,L,Nt
integer, intent(out) :: qmax
integer :: i1, t, i, j, link, nodeIndex, node
integer, allocatable, dimension(:) :: nodeArray
real(kind=8), allocatable, dimension(:) :: randArray

if (allocated(qnet)) deallocate(qnet)
allocate(qnet(N0+Nt))
if (allocated(enet)) deallocate(enet)
allocate(enet(N0+L*Nt,2))
if (allocated(randArray)) deallocate(randArray)
allocate(randArray(L))

!initialize qnet
qnet(1:N0)=2; qnet(N0+1:N0+Nt)=L
!initialize enet
do i1 =1,N0-1
enet(i1,:) = (/ i1,i1+1 /)
end do
enet(N0,:) = (/N0,1/)


!loop through the time sequence
do t = 1,Nt
!nodeArray contains the node indices, with their copies.
!The number of copies of a node = the degree of the node at the previous time interval.
!E.g. nodeArray = [1,1,2,2,3,3,3,4,4, ...]
nodeArray = (/ ((i, j=1,qnet(i)), i = 1,N0+t-1) /)
!new node is assigned with a degree = 1?
!random vector L, with numbers from 0 to 1
!call random_number(randArray)
!loop through the number of new links
!the degree number for the new node is increased by 1 at each iteration
!the degree number for the selected nodes is increased by 1
do link = 1,L
nodeIndex = int(rand(0)*size(nodeArray)) + 1
node = nodeArray(nodeIndex)
qnet(node) = qnet(node) +1
enet(N0+(t-1)*L+link,:) = (/ node, N0+t /)
end do
end do
deallocate(nodeArray, randArray)
qmax = maxval(qnet)
end subroutine generate


subroutine adjacency_matrix()
!Create adjacency matrix corresponding
!to pre-existing edge list
!Initialize the anet matrix
implicit none
integer :: i, node1, node2, numNodes, numEdges
numNodes=size(qnet)
numEdges =size(enet)/2
!loop through the edge list
if (allocated(anet)) deallocate(anet)
allocate(anet(numNodes,numNodes))
anet = 0
do i  = 1,numEdges
node1 = enet(i,1)
node2 = enet(i,2)
anet(node1,node2) = 1
anet(node2,node1) = 1
end do
end subroutine adjacency_matrix


subroutine connectivity(c)
!Compute connectivity of pre-computed network, assume already has anet and qnet
implicit none

real(kind=8), intent(out) :: c
integer :: i, j, numNodes
real(kind=8), allocatable, dimension(:,:) :: D,M
real(kind=8), allocatable, dimension(:) :: work,W
integer :: Info, Lwork, LDA
character :: N, U, Jobz, Uplo


numNodes = size(qnet)
allocate(D(numNodes,numNodes))
D = 0.d0
do i = 1, numNodes
D(i,i) = qnet(i)
end do


M=0.d0
M = D-anet

LDA = numNodes
allocate(work(1))
Lwork = - 1
!print *, 'Lwork=', Lwork
!call dysev for the first time to find optimum value of Lwork
!call dsyev('N','U', numNodes, M, LDA, W, Work, Lwork, Info)
Lwork = work(1)
if (allocated(work)) deallocate(work)
allocate(work(Lwork))
if (allocated(W)) deallocate(W)
allocate(W(numNodes))
!call dysev the second time to find eigenvalues of M
!call dsyev('N','U', numNodes, M, LDA, W, Work, Lwork, Info)
if (info/=0) stop
c=W(2)
deallocate(M, W, D, work)
end subroutine connectivity


subroutine vary_connectivity(N0,Lmax,Nt,carray)
!Compute connectivity for networks with L varying between 1 and Lmax
implicit none
real(kind=8) :: c
integer, intent(in) :: N0,Lmax,Nt
integer :: i, qmax
real(kind=8), dimension(Lmax),intent(out) :: carray

do i=1, Lmax
print *, 'i'
call generate(N0, i, Nt, qmax)
print *, '1'
call adjacency_matrix()
print *, '2'
call connectivity2(c)
print *, '3'
carray(i) = c
print *, 'c=', c
deallocate(anet, qnet, enet)
end do
print *, carray
end subroutine vary_connectivity


end module network
